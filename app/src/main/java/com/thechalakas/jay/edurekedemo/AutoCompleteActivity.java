package com.thechalakas.jay.edurekedemo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;

import static com.thechalakas.jay.edurekedemo.R.id.parent;
import static com.thechalakas.jay.edurekedemo.R.styleable.View;

public class AutoCompleteActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auto_complete);

        AutoCompleteTextView actextview = (AutoCompleteTextView) findViewById(R.id.autocomplete1);

        ArrayAdapter arrayAdapter = new ArrayAdapter(this, R.layout.activity_auto_complete);

        arrayAdapter.add("One");
        arrayAdapter.add("Two");
        arrayAdapter.add("Three");
        arrayAdapter.add("Four");
        arrayAdapter.add("Five");
        arrayAdapter.add("Six");

        //actextview.setThreshold(2);

        actextview.setAdapter(arrayAdapter);


    }
}
