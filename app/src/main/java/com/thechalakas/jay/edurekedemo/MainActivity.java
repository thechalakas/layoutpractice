package com.thechalakas.jay.edurekedemo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //let me get the button object on the UI, here.
        Button button_for_hello = (Button) findViewById(R.id.button_tap);

        Button button_for_linear = (Button) findViewById(R.id.button_linear);

        Button button_for_frame = (Button) findViewById(R.id.button_frame);

        Button button_for_table = (Button) findViewById(R.id.button_table);

        Button button_for_webview = (Button) findViewById(R.id.button_webview);

        Button button_for_autocomplete = (Button) findViewById(R.id.button_autocomplete);

        Button button_for_card = (Button) findViewById(R.id.button_card);

        //listener for the button tap
        button_for_hello.setOnClickListener(new View.OnClickListener()
        {

            //setting the listener (which listens for taps) and make it do what I want it to do
            //in this case, change the text to something else.

            @Override
            public void onClick(View v)
            {
                Log.d("button","button_for_hello click reached");

                TextView textview_code_hello = (TextView) findViewById(R.id.textview_ui_hello);
                String string_to_change = "You just tapped this! Wow, you are such a genius man";
                textview_code_hello.setText(string_to_change);
            }
        });

        button_for_linear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), LinearLayoutActivity.class);
                startActivity(intent);
            }
        });

        button_for_frame.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), FrameLayoutActivity.class);
                startActivity(intent);
            }
        });

        button_for_table.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), TableActivity.class);
                startActivity(intent);
            }
        });

        button_for_webview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), WebviewActivity.class);
                startActivity(intent);
            }
        });

        button_for_autocomplete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), AutoCompleteActivity.class);
                startActivity(intent);
            }
        });

        button_for_card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), CardActivity.class);
                startActivity(intent);
            }
        });

    }//end of onCreate
}
